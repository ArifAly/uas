package com.example.arifuas.Controller;


import com.example.arifuas.model.Customer;
import com.example.arifuas.Repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/uas")
public class CustomerController {
    @Autowired
    CustomerRepository customerRepository;

    @RequestMapping(value = {"","/"})
    public String listCustomer(Model model){
        model.addAttribute("Customer",customerRepository.findAll());
        return "uas/list";
    }

    @GetMapping("/insert")
    public String create(Model model){
        model.addAttribute("Customer",new Customer());
        return "uas/insert";
    }

    @PostMapping("/insert")
    public String addCustomer(@Valid Customer customer, BindingResult result, Model model)
    {
        customerRepository.save(customer);
        model.addAttribute("Customer",customerRepository.findAll());
        return "redirect:/uas";
    }

    @GetMapping("/edit/{id}")
    public String editCustomer(@PathVariable("id") Integer id, Model model){
        Customer customer = customerRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        model.addAttribute("Customer", customer);
        return "uas/edit";
    }

    @PostMapping("/update/{id}")
    public String updateCustomer(@PathVariable("id") Integer id, @Valid Customer customer,
                                 BindingResult result, Model model) {
        if (result.hasErrors()) {
            customer.setId(id);
            return "redirect:/uas";
        }

        customerRepository.save(customer);
        model.addAttribute("Customer", customerRepository.findAll());
        return "redirect:/uas";
    }

    @GetMapping("/delete/{id}")
    public String deleteCustomer(@PathVariable("id") Integer id, Model model) {
        Customer customer = customerRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        customerRepository.delete(customer);
        model.addAttribute("Customer", customerRepository.findAll());
        return "redirect:/uas";
        }

    //Handling Date Error with converting string to date format
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(
                dateFormat, false));
    }
}
