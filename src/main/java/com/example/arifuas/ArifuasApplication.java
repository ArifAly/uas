package com.example.arifuas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArifuasApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArifuasApplication.class, args);
    }

}

